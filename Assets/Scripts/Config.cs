using UnityEngine;

namespace DevTest
{
    [CreateAssetMenu(fileName = "Config", menuName = "ScriptableObjects/Config", order = 0)]
    public class Config : ScriptableObject
    {
        private static Config _Instance;
        public static Config Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = Resources.Load<Config>("Config");
                }

                return _Instance;
            }
        }

        [Tooltip("Application.dataPath + directoryPath")]
        public bool useApplicationDataPath = true;
        public string directoryPath = "";

        public string FullDirectoryPath => useApplicationDataPath ? Application.dataPath + directoryPath : directoryPath;
    }
}
