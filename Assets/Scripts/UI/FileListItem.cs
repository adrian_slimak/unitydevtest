using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

namespace DevTest
{
    public class FileListItem : MonoBehaviour
    {
        [SerializeField] private RawImage fileImage;
        [SerializeField] private TextMeshProUGUI fileNameLabel;
        [SerializeField] private TextMeshProUGUI fileInfoLabel;

        public void Init(FileInfo fileInfo)
        {
            fileNameLabel.text = fileInfo.fileName;
            //fileInfoLabel.text = fileInfo.fileCreationTime.ToString("dd/MM/yyyy mm:HH:ss");
            TimeSpan deltaTime = DateTime.Now - fileInfo.fileCreationTime;
            fileInfoLabel.text = string.Format("{0:D2} days - {1:D2} hrs - {2:D2} mins",
                                                (int)deltaTime.TotalDays, deltaTime.Hours, deltaTime.Minutes);

            fileImage.texture = fileInfo.fileTexture;
            float fileTexRatio = fileInfo.fileTexture.width / (float) fileInfo.fileTexture.height;
            var fileImageRect = fileImage.GetComponent<RectTransform>();
            fileImageRect.sizeDelta = new Vector2(fileImageRect.sizeDelta.y * fileTexRatio, fileImageRect.sizeDelta.y);
        }
    }
}
