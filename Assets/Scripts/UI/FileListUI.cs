using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using TMPro;

namespace DevTest.UI
{
    public class FileListUI : MonoBehaviour
    {
        [Header("Top Panel")]
        [SerializeField] private TMP_InputField dirPathInput;
        [SerializeField] private GameObject dirNotFoundLabel;

        [Header("File List")]
        [SerializeField] private GameObject fileListItemPrefab;
        [SerializeField] private ScrollRect scrollRect;

        private List<FileListItem> _fileListItems;

        private void Awake()
        {
            _fileListItems = new List<FileListItem>();
        }

        private void Start()
        {
            dirPathInput.text = Config.Instance.FullDirectoryPath;

            RefreshFileList();
        }

        private void RefreshFileList()
        {
            bool dirFound = FileLoader.TryToLoadPngFilesFromDir(dirPathInput.text, out var fileInfoList);
            dirNotFoundLabel.SetActive(!dirFound);
            if (!dirFound)
            {
                _fileListItems.ForEach(fileItem => fileItem.gameObject.SetActive(false));
                return;
            }

            PrepareList(fileInfoList.Count);

            for (int i = 0; i < fileInfoList.Count; i++)
                _fileListItems[i].Init(fileInfoList[i]);
        }

        private void PrepareList(int itemCount)
        {
            int missingItems = itemCount - _fileListItems.Count;
            for (int i = 0; i < missingItems; i++)
            {
                var fileListItem = Instantiate(fileListItemPrefab, scrollRect.content).GetComponent<FileListItem>();
                _fileListItems.Add(fileListItem);
            }

            for (int i = 0; i < _fileListItems.Count; i++)
                _fileListItems[i].gameObject.SetActive(i < itemCount);
        }

        public void BttnRefreshClick()
        {
            RefreshFileList();
        }
    }
}
