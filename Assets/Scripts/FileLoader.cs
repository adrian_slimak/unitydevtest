using UnityEngine;
using System.IO;
using System.Collections.Generic;

namespace DevTest
{
    public class FileLoader
    {
        public static bool TryToLoadPngFilesFromDir(string dirPath, out List<FileInfo> fileInfoList)
        {
            if (!Directory.Exists(dirPath))
            {
                Debug.LogWarning($"Directory at path \"{dirPath}\" does't exist!");
                fileInfoList = null;
                return false;
            }

            fileInfoList = new List<FileInfo>();

            DirectoryInfo dir = new DirectoryInfo(dirPath);
            System.IO.FileInfo[] info = dir.GetFiles("*.png");
            foreach (System.IO.FileInfo fileInfo in info)
            {
                byte[] fileData = File.ReadAllBytes(fileInfo.FullName);
                Texture2D fileTex = new Texture2D(2, 2);
                fileTex.LoadImage(fileData);

                fileInfoList.Add(new FileInfo(fileInfo.Name, fileInfo.CreationTime, fileTex));
            }

            return true;
        }
    }
}
