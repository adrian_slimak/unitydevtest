using UnityEngine;
using System;

namespace DevTest
{
    public class FileInfo
    {
        public string fileName { get; }
        public DateTime fileCreationTime { get; }
        public Texture fileTexture { get; }

        public FileInfo(string name, DateTime creationTime, Texture texture)
        {
            fileName = name;
            fileCreationTime = creationTime;
            fileTexture = texture;
        }
    }
}
